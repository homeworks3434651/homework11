package homework11Test;

import org.homework11.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.*;
import java.util.*;

class FamilyControllerTest {

    private final InputStream originalSystemIn = System.in;
    private ByteArrayInputStream inputStream;
    private ByteArrayOutputStream outputStream;
    private static final int MAX_FAMILY_SIZE = 7;

    @BeforeEach
    public void setUp() {
        redirectSystemInput("1\n9\n");
    }

    @AfterEach
    public void tearDown() {
        restoreSystemInput();
    }

    private void restoreSystemInput() {
        System.setIn(originalSystemIn);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    public void setSystemInput(String input) {
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public FamilyController createFamilyController() {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        return new FamilyController(familyService);
    }

    private void redirectSystemInput(String input) {
        inputStream = new ByteArrayInputStream(input.getBytes());
        outputStream = new ByteArrayOutputStream();
        System.setIn(inputStream);
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void testGetFamiliesBiggerThanPrint() {
        setSystemInput("3\n");

        FamilyController familyController = createFamilyController();
        assertThrows(NoSuchElementException.class, () -> familyController.getFamiliesBiggerOrLessThan(true));
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        setSystemInput("4\n");

        FamilyController familyController = createFamilyController();
        familyController.countFamiliesWithMemberNumber();
    }

    @Test
    public void testInvalidInputException() {
        setSystemInput("invalid\n");

        FamilyController familyController = createFamilyController();
        assertThrows(NoSuchElementException.class, () -> familyController.getFamiliesBiggerOrLessThan(true));
    }

    @Test
    void testGetFamiliesBiggerThan() {
        testFamilyFilteringMethod(3, true);
    }

    @Test
    void testGetFamiliesLessThan() {
        testFamilyFilteringMethod(4, false);
    }

    private void testFamilyFilteringMethod(int numberOfMembers, boolean isBiggerThan) {
        FamilyController familyController = createFamilyController();

        family1.addChild(new Man("Bob", "Doe", "13/11/1995"));
        family1.addChild(new Woman("Davida", "Doe", "18/04/2000", family1));
        familyController.saveFamily(family1);

        Man child_family2 = new Man("Ryan", "Smith", "16/05/2004", family2);
        family2.addChild(child_family2);
        familyController.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the appropriate family filtering method
        List<Family> resultFamilies = isBiggerThan ?
                familyController.getFamiliesBiggerThan(numberOfMembers) :
                familyController.getFamiliesLessThan(numberOfMembers);

        // Verify the result and printed output
        assertEquals(1, resultFamilies.size());
        assertTrue(resultFamilies.contains(isBiggerThan ? family1 : family2));

        String isBiggerThanString = """
                Families with numbers of members bigger than 3:\n
                Family1:\n
                    mother:{name='Jane',surname='Doe',birthDate='08/01/1975',iq=0,schedule=null},\n
                    father:{name='John',surname='Doe',birthDate='19/10/1970',iq=0,schedule=null},\n
                    children:\n
                            boy:{name='Bob',surname='Doe',birthDate='13/11/1995',iq=0,schedule=null},\n
                            girl:{name='Davida',surname='Doe',birthDate='18/04/2000',iq=0,schedule=null},\n
                    pets: no pets in this family.
                """;

        String isLessThanString = """
                Families with numbers of members less than 4:\n
                Family2:\n
                    mother:{name='Alice',surname='Smith',birthDate='30/11/1980',iq=0,schedule=null},\n
                    father:{name='Bob',surname='Smith',birthDate='04/11/1975',iq=0,schedule=null},\n
                    children:\n
                            boy:{name='Ryan',surname='Smith',birthDate='16/05/2004',iq=0,schedule=null},\n
                    pets: no pets in this family.
                """;

        String expectedOutput = isBiggerThan ? isBiggerThanString.trim() : isLessThanString.trim();

        String actualOutput = outContent.toString().trim();

        assertEquals(expectedOutput.replaceAll("\\s", ""), actualOutput.replaceAll("\\s", ""));
    }

    @Test
    void testCheckFamilySizeWithinLimit() {
        for (int i = 0; i < MAX_FAMILY_SIZE; i++) {
            family1.addChild(new Man("Child" + i, "Lastname", "01/01/2000"));
        }
        FamilyController familyController = createFamilyController();

        assertThrows(FamilyOverflowException.class, () -> familyController.checkFamilySize(family1));
    }

    @Test
    void testCheckFamilySizeExceedLimit() {
        for (int i = 0; i < MAX_FAMILY_SIZE + 1; i++) {
            family1.addChild(new Man("Child" + i, "Lastname", "01/01/2000"));
        }
        FamilyController familyController = createFamilyController();

        assertThrows(FamilyOverflowException.class, () -> familyController.checkFamilySize(family1));
    }

    @Test
    void testBornChildWithinLimit() {
        FamilyController familyController = createFamilyController();
        familyController.checkFamilySize(family1);
        familyController.bornChild(family1, "BoyName", "GirlName");
        assertEquals(3, family1.countFamily());
    }

    @Test
    void testBornChildExceedLimit() {
        for (int i = 0; i < MAX_FAMILY_SIZE; i++) {
            family1.addChild(new Man("Child" + i, "Lastname", "01/01/2000"));
        }
        FamilyController familyController = createFamilyController();

        assertThrows(FamilyOverflowException.class, () -> familyController.bornChild(family1, "BoyName", "GirlName"));
    }

    @Test
    void testAdoptChildWithinLimit() {
        FamilyController familyController = createFamilyController();
        familyController.checkFamilySize(family1);
        familyController.saveFamily(family1);
        familyController.adoptChild(family1, new Man("AdoptedChild", "Lastname", "01/01/2000"));
        assertEquals(3, family1.countFamily());
    }

    @Test
    void testAdoptChildExceedLimit() {
        for (int i = 0; i < MAX_FAMILY_SIZE; i++) {
            family1.addChild(new Man("Child" + i, "Lastname", "01/01/2000"));
        }
        FamilyController familyController = createFamilyController();
        familyController.saveFamily(family1);
        assertThrows(FamilyOverflowException.class, () -> familyController.adoptChild(family1, new Man("AdoptedChild", "Lastname", "01/01/2000")));
    }

    @Test
    void displayEditFamilyMenu_FamilyOverflowException() {
        // Arrange
        FamilyController familyController = createFamilyController();
        familyController.saveFamily(family1);

        int editFamilyIndex = 1;  // Assuming family at index 1

        String input = editFamilyIndex + "\n1\n";  // Choosing option 1 (Birth)
        setInput(input);

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> familyController.displayEditFamilyMenu(family1, editFamilyIndex));
    }

    // Helper method to set System.in with given input
    private void setInput(String input) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);
    }
}
