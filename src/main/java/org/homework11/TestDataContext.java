package org.homework11;

public class TestDataContext {
    public static FamilyController familyController;

    public static FamilyController init() {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        familyController = new FamilyController(familyService);
        return familyController;
    }
}
