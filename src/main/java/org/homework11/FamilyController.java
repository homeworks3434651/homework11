package org.homework11;

import java.util.*;

public class FamilyController {

    private final FamilyService familyService;
    public static final Scanner scanner = new Scanner(System.in);
    public final int MAX_FAMILY_SIZE = 7;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies(family -> true);
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        List<Family> result = familyService.getFamiliesBiggerThan(numberOfMembers);
        printResult(result, "Families with numbers of members bigger than " + numberOfMembers + ":\n");
        return result;
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        List<Family> result = familyService.getFamiliesLessThan(numberOfMembers);
        printResult(result, "Families with numbers of members less than " + numberOfMembers + ":\n");
        return result;
    }

    public void printResult(List<Family> result, String conditionMessage) {
        if (result.isEmpty()) {
            System.out.println("No families found satisfying the condition.");
        } else {
            System.out.println(conditionMessage);

            List<Family> allFamilies = getAllFamilies();
            for (Family family : result) {
                int index = allFamilies.indexOf(family) + 1;
                System.out.println("Family " + index + ":");
                System.out.println(family.prettyFormat());
            }
        }
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numberOfMembers);
    }

    public void createNewFamily(Man father, Woman mother) {
        familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public void checkFamilySize(Family family) {
        if (family.countFamily() > MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException("Family size exceeds the allowed limit");
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        checkFamilySize(family);
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human child) {
        checkFamilySize(family);
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public void printFamilyById(Family family, int index) {
        if (getFamilyById(index) != null) {
            System.out.println(family.toString().replace("family:\n", "Family: " + (index + 1) + "\n"));
        }
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Set<Pet> pet) {
        familyService.addPet(index, pet);
    }

    public void displayMenu() {
        System.out.println("\n1. Display all families");
        System.out.println("2. Display families with more members than specified");
        System.out.println("3. Display families with fewer members than specified");
        System.out.println("4. Count families with a specific number of members");
        System.out.println("5. Create a new family");
        System.out.println("6. Delete a family by index");
        System.out.println("7. Edit a family by index");
        System.out.println("8. Delete all children older than a specified age");
        System.out.println("9. Exit\n");
    }

    public void console() {
        boolean exit = false;
        TestData.runFamilies(this);
        while (!exit) {
            displayMenu();
            try {
                System.out.print("Enter your choice: ");
                int choice = scanner.nextInt();
                scanner.nextLine();
                switch (choice) {
                    case 1:
                        displayAllFamilies();
                        break;
                    case 2:
                        getFamiliesBiggerOrLessThan(true);
                        break;
                    case 3:
                        getFamiliesBiggerOrLessThan(false);
                        break;
                    case 4:
                        countFamiliesWithMemberNumber();
                        break;
                    case 5:
                        createFamilyWithUserInput();
                        break;
                    case 6:
                        deleteFamilyByIndex();
                        break;
                    case 7:
                        editFamilyAtIndex();
                        break;
                    case 8:
                        deleteChildrenOlderThan();
                        break;
                    case 9:
                        exit = true;
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid integer.");
                scanner.nextLine();
            }
        }
        scanner.close();
    }

    public void getFamiliesBiggerOrLessThan(boolean isBigger) {
        while (true) {
            try {
                System.out.print("Enter the number of members: ");
                int numberOfMembers = scanner.nextInt();
                scanner.nextLine();

                if (numberOfMembers < 0) {
                    throw new IllegalArgumentException("Number of members cannot be negative.");
                }

                if (isBigger) {
                    getFamiliesBiggerThan(numberOfMembers);
                } else {
                    getFamiliesLessThan(numberOfMembers);
                }

                // Break out of the loop if the operation is successful
                break;
            } catch (InputMismatchException e) {
                handleInputMismatchException();
            } catch (IllegalArgumentException e) {
                handleIllegalArgumentException(e.getMessage());
            }
        }
    }

    public void countFamiliesWithMemberNumber() {
        while (true) {
            try {
                System.out.print("Enter the number of members: ");
                int numberOfMembers = scanner.nextInt();
                scanner.nextLine();

                if (numberOfMembers < 0) {
                    throw new IllegalArgumentException("Number of members cannot be negative.");
                }

                System.out.println("There are " + countFamiliesWithMemberNumber(numberOfMembers) +
                        " families with " + numberOfMembers + " members in the family.\n");

                // Break out of the loop if the operation is successful
                break;
            } catch (InputMismatchException e) {
                handleInputMismatchException();
            } catch (IllegalArgumentException e) {
                handleIllegalArgumentException(e.getMessage());
            }
        }
    }

    public void createFamilyWithUserInput() {
        try {
            String motherName = promptValidName("Enter the name of the mother");
            String motherSurname = promptValidName("Enter the surname of the mother");
            int motherBirthYear = promptInt("Enter the birth year of the mother", 1970, 2006);
            int motherBirthMonth = promptInt("Enter the birth month of the mother", 1, 12);
            int motherBirthDay = promptInt("Enter the birth day of the mother", 1, getDaysInMonth(motherBirthYear, motherBirthMonth));
            int motherIQ = promptInt("Enter the IQ of the mother", 70, 230);

            String fatherName = promptValidName("Enter the name of the father");
            String fatherSurname = promptValidName("Enter the surname of the father");
            int fatherBirthYear = promptInt("Enter the birth year of the father", 1970, 2005);
            int fatherBirthMonth = promptInt("Enter the birth month of the father", 1, 12);
            int fatherBirthDay = promptInt("Enter the birth day of the father", 1, getDaysInMonth(fatherBirthYear, fatherBirthMonth));
            int fatherIQ = promptInt("Enter the IQ of the father", 70, 230);

            String formattedFatherBirthDate = String.format("%02d/%02d/%d", fatherBirthDay, fatherBirthMonth, fatherBirthYear);
            Man father = new Man(fatherName, fatherSurname, formattedFatherBirthDate, fatherIQ);

            String formattedMotherBirthDate = String.format("%02d/%02d/%d", motherBirthDay, motherBirthMonth, motherBirthYear);
            Woman mother = new Woman(motherName, motherSurname, formattedMotherBirthDate, motherIQ);

            this.createNewFamily(father, mother);
            System.out.println("New family: ");
            printFamilyById(getFamilyById(getAllFamilies().size() - 1), getAllFamilies().size() - 1);
        } catch (InputMismatchException e) {
            System.out.println("Error: Invalid input. Please enter valid data.");
            scanner.nextLine();
        }
    }

    public int getDaysInMonth(int year, int month) {
        if (month == 2) {
            // Check if it's a leap year
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                return 29; // Leap year
            } else {
                return 28; // Non-leap year
            }
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30; // April, June, September, November have 30 days
        } else {
            return 31; // January, March, May, July, August, October, December have 31 days
        }
    }

    public String promptValidName(String message) {
        while (true) {
            try {
                String input = promptString(message);
                validateName(input);
                return formatName(input);
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public void validateName(String name) {
        if (name.matches(".*\\d.*")) {
            throw new IllegalArgumentException("Name cannot contain digits.");
        }

        if (!name.matches("^[a-zA-Z]+(-[a-zA-Z]+)*(\\s[a-zA-Z]+(-[a-zA-Z]+)*)*$")) {
            throw new IllegalArgumentException("Name must contain only latin letters, hyphens, and spaces.");
        }
    }

    public String formatName(String name) {
        String[] words = name.split("[\\s-]+");
        StringBuilder formattedName = new StringBuilder();
        for (String word : words) {
            if (formattedName.length() > 0) {
                formattedName.append(" ");
            }
            formattedName.append(word.substring(0, 1).toUpperCase())
                    .append(word.substring(1).toLowerCase());
        }
        return formattedName.toString();
    }

    public void deleteFamilyByIndex() {
        while (true) {
            try {
                System.out.print("Enter the index of the family to delete (or -1 to exit): ");

                if (!scanner.hasNext()) {
                    break;  // exit the loop if there's no more input
                }

                int inputIndex = scanner.nextInt();

                if (inputIndex == -1) {
                    break;  // exit the loop if -1 is entered
                }

                if (inputIndex <= 0 || inputIndex > getAllFamilies().size()) {
                    throw new IllegalArgumentException("Invalid index. Please enter a valid index.");
                }

                Family deletedFamily = getFamilyById(inputIndex - 1); // Adjusting to 0-based index
                if (deletedFamily != null) {
                    deleteFamilyByIndex(inputIndex - 1);  // Delete by 0-based index
                    System.out.println("Family with index " + inputIndex + " deleted.");
                } else {
                    System.out.println("Error: Family with index " + inputIndex + " not found.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid index (integer).");
                scanner.nextLine(); // Consume newline to avoid an infinite loop
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public int editFamilyAtIndex() {
        int editFamilyIndex = -2;
        while (true) {
            System.out.print("Enter the index of the family to edit (or -1 to exit): ");
            try {
                editFamilyIndex = scanner.nextInt();
                scanner.nextLine();

                if (editFamilyIndex == -1) {
                    return -1;  // exit the method if -1 is entered
                }

                if (editFamilyIndex <= 0 || editFamilyIndex > getAllFamilies().size()) {
                    throw new IllegalArgumentException("Invalid index. Please enter a valid index.");
                }

                Family familyToEdit = getFamilyById(editFamilyIndex - 1);
                displayEditFamilyMenu(familyToEdit, editFamilyIndex);
                return editFamilyIndex;  // exit the loop after successfully displaying the menu
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid index (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public void displayEditFamilyMenu(Family familyToEdit, int editFamilyIndex) {
        while (true) {
            System.out.println("1. Give birth to a child");
            System.out.println("2. Adopt a child");
            System.out.println("3. Return to the main menu");
            System.out.print("Enter your choice: ");

            int editChoice;
            try {
                editChoice = scanner.nextInt();
                scanner.nextLine();

                if (familyToEdit.countFamily() == MAX_FAMILY_SIZE) {
                    throw new FamilyOverflowException("Family size exceeds the allowed limit");
                }

                switch (editChoice) {
                    case 1:
                        editFamilyByBirth(familyToEdit, editFamilyIndex);
                        break;
                    case 2:
                        editFamilyByAdoption(familyToEdit, editFamilyIndex);
                        break;
                    case 3:
                        return; // Return to the main menu
                    default:
                        System.out.println("Invalid choice. Please enter a valid choice.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid choice (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            } catch (FamilyOverflowException e) {
                System.out.println("Error: " + e.getMessage());
                return;
            }
        }
    }

    public void editFamilyByBirth(Family familyToEdit, int editFamilyIndex) {
        String boyName = promptValidName("Enter the name for the child (boy)");
        String girlName = promptValidName("Enter the name for the child (girl)");
        this.bornChild(familyToEdit, boyName, girlName);
        System.out.println("Editing family: ");
        printFamilyById(getFamilyById(editFamilyIndex - 1), editFamilyIndex - 1);
    }

    public void editFamilyByAdoption(Family familyToEdit, int editFamilyIndex) {
        String childGender = promptGender("Enter the gender of the child (Man/Woman)");
        String childName = promptValidName("Enter the name of the child");
        int childBirthYear = promptInt("Enter the birth year of the child", 2009, 2024);
        int childIQ = promptInt("Enter the IQ of the child", 70, 230);
        if (childGender.equalsIgnoreCase("Man")) {
            Man son = new Man(childName, familyToEdit.getFather().getSurname(), childBirthYear, childIQ, familyToEdit);
            this.adoptChild(familyToEdit, son);
        } else if (childGender.equalsIgnoreCase("Woman")) {
            Woman daughter = new Woman(childName, familyToEdit.getFather().getSurname(), childBirthYear, childIQ, familyToEdit);
            this.adoptChild(familyToEdit, daughter);
        }
        System.out.println("Editing family: ");
        printFamilyById(getFamilyById(editFamilyIndex - 1), editFamilyIndex - 1);
    }

    public String promptGender(String message) {
        while (true) {
            try {
                System.out.print(message + ": ");
                String gender = scanner.nextLine().trim();

                if (gender.equalsIgnoreCase("Man") || gender.equalsIgnoreCase("Woman")) {
                    return gender;
                } else {
                    throw new IllegalArgumentException("Invalid gender. Please enter 'Man' or 'Woman'.");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public String promptString(String message) {
        System.out.print(message + ": ");
        return scanner.nextLine();
    }

    public int promptInt(String message, int minValue, int maxValue) {
        int value;
        do {
            value = validateInput(message + " (" + minValue + " - " + maxValue + "): ", minValue, maxValue);
        } while (value < minValue || value > maxValue);

        return value;
    }

    public int validateInput(String prompt, int minValue, int maxValue) {
        int value;
        while (true) {
            try {
                System.out.print(prompt);
                while (!scanner.hasNextInt()) {
                    System.out.print("Invalid input. Please enter a valid integer: ");
                    scanner.next(); // consume the invalid input
                }
                value = scanner.nextInt();
                scanner.nextLine(); // consume the newline

                if (value < minValue || value > maxValue) {
                    System.out.println("Value must be between " + minValue + " and " + maxValue + ".");
                } else {
                    break; // valid input, exit the loop
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid integer.");
                scanner.nextLine(); // consume the invalid input
            }
        }
        return value;
    }

    public void deleteChildrenOlderThan() {
        while (true) {
            try {
                System.out.print("Enter the age to delete children older than (or -1 to exit): ");
                int ageDeleteAllChildrenOlderThan = scanner.nextInt();
                scanner.nextLine();

                if (ageDeleteAllChildrenOlderThan == -1) {
                    break;  // exit the loop if -1 is entered
                }

                if (ageDeleteAllChildrenOlderThan < 0) {
                    throw new IllegalArgumentException("Age cannot be negative.");
                }

                deleteAllChildrenOlderThan(ageDeleteAllChildrenOlderThan);
                System.out.println("Children older than " + ageDeleteAllChildrenOlderThan + " have been deleted.");
                break;  // exit the loop after successful deletion
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid age (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public void handleInputMismatchException() {
        System.out.println("Error: Invalid input. Please enter a valid integer.");
        scanner.nextLine();
    }

    public void handleIllegalArgumentException(String message) {
        System.out.println("Error: " + message);
    }
}
