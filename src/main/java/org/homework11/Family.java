package org.homework11;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class Family {
    private Man father;
    private Woman mother;
    private List<Human> children;
    private Set<Pet> pets;

    private boolean parentsSet;

    static {
        System.out.println("Family class is loaded.");
    }

    {
        System.out.println("A new Family object is created.");
    }

    public Family(Man father, Woman mother) throws IllegalArgumentException {
        if (father == null && mother == null) {
            throw new IllegalArgumentException("Invalid family creation. Please ensure both parents are provided and not null.");
        }

        if (father != null && mother != null) {
            if (!checkParentTypes(father, mother)) {
                throw new IllegalArgumentException("Error: Father must be of type Man, and mother must be of type Woman.");
            }

            if (father.equals(mother)) {
                throw new IllegalArgumentException("Error: Father and mother cannot be the same person.");
            }

            if (father.getFamily() != null || mother.getFamily() != null) {
                throw new IllegalArgumentException("Error: One or both parents are already part of another family.");
            }

            this.father = father;
            this.father.setFamily(this);
            this.mother = mother;
            this.mother.setFamily(this);
            this.children = new ArrayList<>();
            this.pets = new HashSet<>();
            parentsSet = true;
        } else if (father == null) {
            throw new IllegalArgumentException("Error: Father cannot be null.");
        } else {
            throw new IllegalArgumentException("Error: Mother cannot be null.");
        }
    }

    private boolean checkParentTypes(Man father, Woman mother) {
        return father instanceof Man && mother instanceof Woman;
    }

    public boolean addChild(Human child) {
        if (children.contains(child)) {
            System.out.println("Error: This person is already part of the family.");
            return false;
        }

        if (child instanceof Man || child instanceof Woman) {
            if (child.getFamily() == null || child.getFamily().equals(this)) {
                child.setFamily(this);
                children.add(child);
                this.mergeSchedule(child.getSchedule());
                return true;
            } else {
                System.out.println("Error: This person is already part of another family.");
                return false;
            }
        } else {
            System.out.println("Error: Every child must be Man or Woman.");
            return false;
        }
    }

    private void mergeSchedule(Map<DayOfWeek, List<String>> childSchedule) {
        childSchedule.forEach((day, activities) -> DayOfWeek.mergeActivities(day, activities));
    }

    public boolean deleteChild(Human child) {
        if (child == null) {
            System.out.println("Child is null. Cannot delete null child.");
            return false;
        }

        if (children != null) {
            if (children.remove(child)) {
                System.out.println("Child deleted successfully.");
                System.out.println("Family after deleting child:");
                System.out.println(this);
                return true; // Return true if deletion is successful
            } else {
                System.out.println("Child not found in the family.");
                return false; // Return false if child not found
            }
        }

        return false; // Return false if children is null
    }

    public boolean deleteChild(int index) {
        List<Human> sortedChildren = getSortedChildren();

        if (sortedChildren != null && index >= 0 && index < sortedChildren.size()) {
            Human childToRemove = sortedChildren.remove(index);

            if (childToRemove != null) {
                childToRemove.setFamily(null); // Detach child from the family
                children.remove(childToRemove); // Remove from the original list

                System.out.println("Child at index " + index + " deleted successfully.");
                System.out.println("Family after deleting child at index " + index + ":");
                System.out.println(this);
                return true;
            } else {
                System.out.println("Could not delete child at index " + index + ".");
                return false;
            }
        } else {
            System.out.println("Could not delete child at index " + index + ".");
            return false;
        }
    }

    public Human getChild(int index) {
        if (index < 0 || index >= children.size()) {
            throw new IndexOutOfBoundsException("Invalid index: " + index);
        }
        return children.get(index);
    }

    public int countFamily() {
        long parentsCount = Stream.of(father, mother).filter(Objects::nonNull).count();
        long childrenCount = children != null ? children.size() : 0;
        return (int) (parentsCount + childrenCount);
    }

    public Set<Pet> getPets() {
        return pets != null ? new HashSet<>(pets) : Collections.emptySet();
    }

    public void setPets(Set<Pet> pets) {
        if (this.pets == null) {
            this.pets = new HashSet<>();
        }

        if (pets != null) {
            this.pets.addAll(pets);
        }
    }

    private void setParent(Human parent, Class<? extends Human> parentClass) {
        if (!parentsSet) {
            if (parentClass.isInstance(parent)) {
                if (parentClass == Man.class) {
                    this.father = (Man) parent;
                } else if (parentClass == Woman.class) {
                    this.mother = (Woman) parent;
                }
                parent.setFamily(this);
                parentsSet = true;
            } else {
                System.out.println("Error: Parent must be an instance of " + parentClass.getSimpleName() + ".");
            }
        } else {
            if ((this.father == null || this.mother == null) && parentClass.isInstance(parent)) {
                if (this.father == null && parentClass == Man.class) {
                    this.father = (Man) parent;
                } else if (this.mother == null && parentClass == Woman.class) {
                    this.mother = (Woman) parent;
                }
                parent.setFamily(this);
            } else {
                System.out.println("Error: Family already has two parents.");
            }
        }
    }

    public void setFather(Human father) {
        setParent(father, Man.class);
    }

    public Man getFather() {
        return father;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        setParent(mother, Woman.class);
    }

    private List<Human> getSortedChildren() {
        if (children != null && !children.isEmpty()) {
            return children.stream()
                    .sorted(Comparator.comparingLong(Human::getBirthDateUnix))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public List<Human> getChildren() {
        return getSortedChildren();
    }

    public void setChildren(List<Human> newChildren) {
        for (Human child : newChildren) {
            if (child instanceof Man || child instanceof Woman) {
                child.setFamily(this);
                children.add(child);
            } else {
                System.out.println("Error: Every child must be Man or Woman.");
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;

        Family family = (Family) o;

        boolean parentsEqual = Objects.equals(father, family.father) &&
                Objects.equals(mother, family.mother);

        boolean childrenEqual = Objects.equals(children, family.children);

        return parentsEqual && childrenEqual;
    }

    @Override
    public int hashCode() {
        return Objects.hash(father, mother, children);
    }

    public String prettyFormat() {
        StringBuilder sb = new StringBuilder();

        // Mother
        sb.append("   mother: ").append(mother.prettyFormat());

        // Father
        sb.append("   father: ").append(father.prettyFormat());

        // Children
        if (!getChildren().isEmpty()) {
            sb.append("   children:\n");

            for (Human child : getChildren()) {
                if (child instanceof Man) {
                    sb.append("           boy:  ").append(child.prettyFormat());
                } else {
                    sb.append("           girl: ").append(child.prettyFormat());
                }
            }
        } else {
            sb.append("   children: no children in this family,\n");
        }

        // Pets
        if (pets != null && !pets.isEmpty()) {
            sb.append("   pets: ").append(pets.stream().map(Pet::prettyFormat).collect(Collectors.toList())).append("\n");
        } else {
            sb.append("   pets: no pets in this family.\n");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("family:\n");
        result.append(prettyFormat());
        return result.toString();
    }
}
